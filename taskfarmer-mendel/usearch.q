#!/bin/sh

#PBS -N 9468
#PBS -q regular
#PBS -l mppwidth=3600,walltime=01:00:00
#PBS -A m1045
#PBS -V

cd $PBS_O_WORKDIR 

#module load blast taskfarmer hmmsort hmmer
#module use --append /usr/common/tig/Modules/modulefiles
#module load taskfarmer/1.5


#export BASE="/scratch/scratchdirs/apati/MER_FunctionPrediction/FuncPredWork/rpsblast"
export PATH=$PATH:$BASE
#export BATCHSIZE=640
export SERVER_TIMEOUT=36000
#export WORK_DIR=/scratch/scratchdirs/sulsj/taskfarmer-test/2012.06.14-taskfarmer-test/amrita_code/rpsblast/test1
export MY_TFRPSBLAST=/usr/common/tig/taskfarmer/1.5/bin/tfrpsblast 
export TFUSEARCH=/usr/common/tig/taskfarmer/1.5/bin/tfusearch
DB=1
ID=9468

INPUT=`pwd`/$ID.unassembled_illumina.faa
export THREADS=4

for db in $(seq 1 20) ; do
  export Q=/scratch/scratchdirs/apati/MER_FunctionPrediction/FuncPredDBs/IMGNR/img.faa.${DB}.udb
  ln -s $INPUT $INPUT.${DB}
  `pwd`/tfusearch -query $INPUT.${DB} --udb /scratch/scratchdirs/apati/MER_FunctionPrediction/FuncPredDBs/IMGNR/img.faa.${DB}.udb -blast6out `pwd`/$ID.usearch.$DB.b6  --iddef 4 -id 0 -local --minlen 20 --trunclabels  --ka_dbsize 700000000 --maxhits 20 -threads 6 -quiet
done


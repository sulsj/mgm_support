#!/bin/bash -l

#$ -S /bin/bash
#$ -cwd
#$ -l h_rt=01:00:00
#$ -l ram.c=4G
#$ -l h_vmem=4G
##$ -pe pe_16 16
#$ -q mendel.q
#$ -t 1-2

module use --append /usr/common/tig/Modules/modulefiles
module use --append /usr/common/jgi/Modules/modulefiles
module load taskfarmer/1.5
module load hmmer
module load python

NPROCS=16
ID=9190

echo "Worker#: $SGE_TASK_ID"
for i in {1..16}
do
    NEW_SGE_TASK_ID=$(( (($SGE_TASK_ID-1)*16) + $i )) 
    echo "new id = $NEW_SGE_TASK_ID"
    #python ./worker.py test_queue &
    #`pwd`/tfsplit_sulsj `pwd`/9190.unassembled_illumina.faa `pwd`/9190.split & 
    runcommands.sh ./test.lst & 
done

wait

#`pwd`/tfsplit ./9190.unassembled_illumina.faa ./9190.split

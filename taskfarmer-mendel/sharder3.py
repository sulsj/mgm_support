#!/usr/bin/env python

import sys
from cStringIO import StringIO
import os
import argparse

udbs = ["img.faa.1.udb", "img.faa.2.udb", "img.faa.3.udb", "img.faa.4.udb", "img.faa.5.udb", "img.faa.6.udb", "img.faa.7.udb", "img.faa.8.udb", "img.faa.9.udb", "img.faa.10.udb"]

class FastaReader(object):
    """Class that supports an input iterator protocol for a FASTA file.
    Example that prints an exact copy of the input file:
    for rec in FastaReader(open('seq.fsa','r')).records():
        print rec.header(),
        for line in rec.seqLines():
            print line,
    Instead of rec.seqLines(), you can use the methods which post-process the
    raw sequences lines: seqChunks(), seqArrays(), sequence().
    """

    def __init__(self,infile):
        if not hasattr(infile,"readline"):
            infile = openCompressed(infile,'r')
            self.ownInfile = True
        else:
            self.ownInfile = False
        self.infile = infile
        self.freshHdr = False
        self.maxLineLen = 0
        
    def records(self):
        infile = self.infile
        while True:
            if self.freshHdr:
                self.freshHdr = False
                yield self
                continue
            line = infile.readline()
            
            if not line:
                return
            # skip blank lines
            elif line.isspace():
                continue
            elif line.startswith(">"):
                self.hdr = line
                yield self
    
    def header(self):
        assert self.hdr.startswith('>')
        return self.hdr
        
    def getNCBI_Id(self):
        """Assume that header starts with '>gi|1234567|' and return the string id from second field."""
        return self.hdr.split('|',2)[1]
    
    def getNCBI_GI(self):
        return int(self.getNCBI_Id())

    def getSimpleId(self):
        """Assume that header starts with '>string_no_spaces ' and return that string."""
        return self.hdr.split(None,1)[0][1:]

    def seqLines(self):
        infile = self.infile
        while True:
            line = infile.readline()
            if not line:
                break
            elif line.isspace():
                continue
            elif line.startswith(">"):
                self.hdr = line
                self.freshHdr = True
                return
            self.maxLineLen = max(self.maxLineLen,len(line)-1)
            yield line

    def seqChunks(self,queryLen):
        seq = StringIO()
        for line in self.seqLines():
            seq.write(line.rstrip("\n"))
            if seq.tell() >= queryLen:
                yield seq.getvalue()
                seq.close()
                seq = StringIO()
        if seq.tell() > 0:
            yield seq.getvalue()
        seq.close()

    #def seqArrays(self,queryLen):
        #for s in self.seqChunks(queryLen):
            #yield numpy.fromstring(s,dtype='S1')

    def sequence(self,format='str'):
        seq = StringIO()
        for line in self.seqLines():
            seq.write(line.rstrip("\n"))
        s = seq.getvalue()
        seq.close()
        #if format == 'array':
            #s = numpy.fromstring(s,dtype='S1')
        return s

    def seqLen(self):
        n = 0
        for line in self.seqLines():
            n += len(line) - 1
            if not line.endswith("\n"):
                n += 1
        return n

    def lineLen(self):
        return self.maxLineLen

    def close(self):
        if self.ownInfile:
            self.infile.close()
 

if __name__ == '__main__':

    desc = u'fastasplit'
    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument('-i', '--input', help='input multifasta file', dest='inputFile', required=True)
    parser.add_argument('-s', '--split', help='number of splits', dest='nSplits', required=True)
    parser.add_argument('-d', '--dest', help='destination directory', dest='destDir', required=True)
    args = parser.parse_args()

    seqFileName = args.inputFile 
    nBlocks = int(args.nSplits)
    destDir = args.destDir

    nSeq = 0
    bid = 1         ## block id
    seqs = ""
    dirNames = []

    for i in range(nBlocks):
        dName = (seqFileName + "_" + "%06d" + "_" + udbs[0]) % (i)
        fPath = os.path.join(destDir, dName)
        print fPath
        try:
            if not os.path.exists(fPath):
                os.makedirs(fPath)
        except:
            print >> sys.stderr, "Failed to create directories."
            sys.exit(0)
        dirNames.append(fPath)
    print dirNames
    
    fdList = []
    try:
        for f in dirNames:
            fastaFileName = os.path.join(f, "data.faa")
            fdList.append(open(fastaFileName, 'w'))

        for rec in FastaReader(open(seqFileName, 'r')).records():
            defLine = rec.header().strip()
            currSeq = rec.sequence().strip()
            fdList[nSeq % nBlocks].write(defLine + "\n" + currSeq + "\n")
            nSeq += 1
    finally:
        for f in fdList:
            f.close()

    for d in range(nBlocks):
        for i in range(1,len(udbs)):
            dName = (seqFileName + "_" + "%06d" + "_" + udbs[i]) % (d)
            fPath = os.path.join(destDir, dName)
            print fPath
            if not os.path.exists(fPath):
                os.makedirs(fPath)
            destFile = fPath + "/data.faa"
            if not os.path.lexists(destFile):
                orig = (seqFileName + "_" + "%06d" + "_" + udbs[0]) % (d)
                os.symlink("../"+orig, destFile)

    print "Total number of input sequences = ", nSeq

## EOF

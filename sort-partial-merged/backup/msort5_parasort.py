#!/usr/bin/env python
import sys
import glob
from collections import defaultdict
import operator
import multiprocessing as mp
import math
import time
import os
import Queue
import threading

#class MyManager(BaseManager):
#    pass
#
#MyManager.register('defaultdict', defaultdict, DictProxy)

def load_file(f, d):
    """
    """
    print "Reading ", f
    fd = open(f, 'r')
    while 1:
        lines = fd.readlines(100000) ## batch readline
        if not lines:
            break
        for l in lines:
            #k = l.strip().split()[0]
            #v = l.strip()
            #d[k].append(v)
            stripped = l.strip()                    
            t = stripped.split()
            k = t[0]
            #v = "%s %08.2f %f %d %s" % (t[0], float(t[11]), float(t[10]), int(t[3]), stripped)
            d[k].append(stripped)
    fd.close()
    #print d
    

def load_dict(dirName, d):
    """
    """
    for f in glob.glob(dirName+'/*.db*'): ## filenames for *.db??
    #for f in glob.glob(dirName+'/*.txt'): ## filenames for *.db??
        print "Reading ", f
        fd = open(f, 'r')
        while 1:
            lines = fd.readlines(100000) ## batch readline
            if not lines:
                break
            for l in lines:
                #k = l.strip().split()[0]
                #v = l.strip()
                #d[k].append(v)
                ##
                ## Text newKey=new Text( String.format("%s %08.2f %f %d", a[0],
                ## Float.parseFloat(a[11]), // bitscore
                ## Float.parseFloat(a[10]), //evalue
                ## Integer.parseInt(a[3]) // alignment length
                ## )  );
                ##
                stripped = l.strip()                    
                t = stripped.split()
                k = t[0]
                v = "%s %08.2f %f %d %s" % (t[0], float(t[11]), float(t[10]), int(t[3]), stripped)
                d[k].append(v)
        fd.close()
    #print d

def worker(v, out_q):
    """  
    """
    #outdict = {}
    #for n in nums:
    #    outdict[n] = factorize_naive(n)
    #out_q.put(outdict)
    #print "key list = ", kl
    #for k, v in d.items():          
    #    if k in kl:
    #        print v
    #        v.sort(key=lambda s: (s.split()[0], -float(s.split()[-1]))) ## sort by gene_id ASC, bitscore DESC
    #        print v
    #print v
    #v.sort(key=lambda s: (s.split()[0], -float(s.split()[-1]))) ## sort by gene_id ASC, bitscore DESC
    
    ##
    ## Float.parseFloat(a[11]), // bitscore
    ## Float.parseFloat(a[10]), //evalue
    ## Integer.parseInt(a[3])   // alignment length
    ##
    ## Sorting
    ## 1:) Make sure you have only results from the same sequence of course
    ## (e.g. S_N403_prefert11DRAFT_30000001501)
    ## 2.) Sort by bitscore (highest first)
    ## 3) If two hits have the same bitscore, continue sorting by evalue
    ## (smallest first)
    ## 4) and if even that's the same, then continue sorting by the alignment
    ## length (longest first)
    ##
    #v.sort(key=lambda s: (s.split()[0],         # gene_id ASC
    #                      -float(s.split()[1]), # bitscore DESC
    #                      float(s.split()[2]),  # evalue ASC
    #                      -int(s.split()[3])    # alignlen DESC
    #       ))    
    v.sort(key=lambda s: (s.split()[0],         # gene_id ASC
                          -float(s.split()[11]), # bitscore DESC
                          float(s.split()[10]),  # evalue ASC
                          -int(s.split()[3])    # alignlen DESC
           ))
    
    #print v
    out_q.put(v)
        
def para_sort(d, nprocs):
    # Each process will get 'chunksize' nums and a queue to put his out
    # dict into
    manager = mp.Manager()
    out_q = manager.Queue()    
    #out_q = mp.Queue()    
    pool = mp.Pool(nprocs)

    #keyList = d.keys()
    #chunksize = int(math.ceil(len(keyList) / float(nprocs)))
    #print "Chunk size = ", chunksize
    
    procs = []
    #for i in range(nprocs):
    #    p = mp.Process(target=worker,
    #                   args=(d, keyList[chunksize * i:chunksize * (i + 1)], out_q))
    #    procs.append(p)
    #    p.start()
    
    print "  Firing off workers"
    for k, v in d.items():
        proc = pool.apply_async(worker, (v, out_q))
        procs.append(proc)

    # Collect all results into a single result dict. We know how many dicts
    # with results to expect.
    #resultdict = {}
    #for i in range(nprocs):
        #resultdict.update(out_q.get())

    # Wait for all worker processes to finish
    #for p in procs:
        #p.join()
    
    ## collect results from the workers through the pool result queue
    print "  Collecting results from the workers"
    for proc in procs:
        sortedList = out_q.get()
        d[sortedList[0].split()[0]] = sortedList

    pool.close()
    pool.join()
    
    #return resultdict

def serial_sort(d):
    for k, v in d.items():
        v.sort(key=lambda s: (s.split()[0],         # gene_id ASC
                              -float(s.split()[11]), # bitscore DESC
                              float(s.split()[10]),  # evalue ASC
                              -int(s.split()[3])    # alignlen DESC
             ))

def chunks(l, n):
    """ Yield successive n-sized chunks from l.
    """
    for i in xrange(0, len(l), n):
        yield l[i:i+n]

class sort_thread(threading.Thread):
    def __init__(self, tq, rq):
        threading.Thread.__init__(self)
        self.taskqueue = tq
        self.resultqueue = rq
        
    def run(self):
         while True:
            listToSort = self.taskqueue.get()
            if listToSort is None:
                break # reached end of queue
            listToSort.sort(key=lambda s: (s.split()[0],         # gene_id ASC
                                        -float(s.split()[11]), # bitscore DESC
                                        float(s.split()[10]),  # evalue ASC
                                        -int(s.split()[3])    # alignlen DESC
             ))
            self.resultqueue.put(listToSort)
            #self.taskqueue.task_done()

def para_sort_thread(d, nprocs):
    
    taskQueue = Queue.Queue()
    resultQueue = Queue.Queue()
    
    for i in range(nprocs):
        t = sort_thread(taskQueue, resultQueue)
        t.setDaemon(True)
        t.start()
        
    print "  Firing off workers"
    for k, v in d.items():
        taskQueue.put(v)
        
    #taskQueue.join()
    
    for i in range(nprocs):
        taskQueue.put(None) # add end-of-queue markers

    print "  Collecting results..."
    while not resultQueue.empty():
        sortedList = resultQueue.get()
        d[sortedList[0].split()[0]] = sortedList

    
if __name__ == "__main__":
    
    inFileName = sys.argv[1]
    outFileName = sys.argv[2]
    
    if not os.path.isfile(outFileName):
        d = defaultdict(list)
        
        start = time.clock()
        print "Loading dict..."
        #load_dict(dirName, d)
        load_file(inFileName, d)
        print "Loading done, ", time.clock() - start
                
        start = time.clock()
        print "Sorting..."
        print "  Cpu count = ", mp.cpu_count()
        ###################################
        #para_sort(d, mp.cpu_count()/32)
        #serial_sort(d)
        para_sort_thread(d, mp.cpu_count())
        ###################################
        print "Sorting done, ", time.clock() - start
    
        start = time.clock()
        print "Writing..."
        tmpOutFileName = outFileName + ".tmp"
        outfd = open(tmpOutFileName, 'w')
        for k, v in d.items():
            for h in v:
                t = h.split()
                line = "%s %08.2f %f %d %s" % (t[0], float(t[11]), float(t[10]), int(t[3]), h)
                outfd.write("%s\n" % line)
        outfd.close()
        print "Writing done, ", time.clock() - start
        
        #time.sleep(10)
        os.rename(tmpOutFileName, outFileName)
    else:
        print "Already done ", outFileName

#EOF 


#!/usr/bin/env python
import sys
import glob
import os
#import multiprocessing as mp
#import Queue


def get_num_line(fn):
    f = open(fn)
    lines = 0
    buf_size = 1024 * 1024
    read_f = f.read # loop optimization

    buf = read_f(buf_size)
    while buf:
        lines += buf.count('\n')
        buf = read_f(buf_size)
    f.close()

    #outq.put(lines)
    return lines


#def get_line_nums_multiproc(f1, f2):
#    manager = mp.Manager()
#    out_q = manager.Queue()    
#    pool = mp.Pool(nprocs)    
#    proc1 = pool.apply_async(get_num_line, (f1))
#    proc2 = pool.apply_async(get_num_line, (f2))
#    size = out_q.get()
#    proc2 = out_q.get()
#    pool.close()
#    pool.join()
    

if __name__ == '__main__':
    #srcDirName=sys.argv[1]
    srcFileNames=(sys.argv[1],)
    destDirName=sys.argv[2]
    #srcFileNames = glob.glob(srcDirName + "/*.udbout")
    #dstFileNames = glob.glob(destDirName + "/*.udbout")
    #print "num files in src = ", len(srcFileNames)
    #print "num files in dst = ", len(dstFileNames)

    for f in srcFileNames:
        
        fn1 = os.path.basename(f) + "_NOT_FOUND"
        fl1 = glob.glob(os.path.join(destDirName, fn1)+"*")
        dstFileName = os.path.join(destDirName, os.path.basename(f))
        fn2 = dstFileName + "_QC_ERROR_read_"
        fl2 = glob.glob(fn2+"*")
        fn3 = dstFileName + "_QC_OK_read_"
        fl3 = glob.glob(fn3+"*")
        if len(fl1) or len(fl2) or len(fl3):
            print "Already done"
            continue
        else:
            ## no output file found in dest dir
            if not os.path.isfile(os.path.join(destDirName, os.path.basename(f))):
                fn = os.path.basename(f) + "_NOT_FOUND"
                print "Create a not_found file, ", fn
                fd = open(os.path.join(destDirName, fn), "w")
                fd.write("not found\n")
                fd.close()
            else:
                ## out file found. check the line num.
                dstFileName = os.path.join(destDirName, os.path.basename(f))
                dstLineNum = get_num_line(dstFileName)
                srcLineNum = get_num_line(f)
                if srcLineNum != dstLineNum:
                    print "ERROR ", f, srcLineNum, dstLineNum
                    fn = dstFileName + "_QC_ERROR_read_" + str(srcLineNum) + "_written_" + str(dstLineNum)
                    print "Create an error file, ", fn
                    fd = open(fn, "w")
                    fd.write("error\n")
                    fd.close()
                    os.rename(dstFileName, dstFileName+".ERROR")
                else:
                    print "OK ", f, srcLineNum, dstLineNum
                    fn = dstFileName + "_QC_OK_read_" + str(srcLineNum) + "_written_" + str(dstLineNum)
                    print "Create OK file, ", fn
                    fd = open(fn, "w")
                    fd.write("ok\n")
                    fd.close()
        
## EOF
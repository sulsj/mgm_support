#!/usr/bin/env python
import sys
import glob
import os

srcDirName=sys.argv[1]
destDirName=sys.argv[2]

fnames = glob.glob(srcDirName + "/*.udbout")
#print "num files = ", len(fnames)

for f in fnames:
    print "/jgi/tools/bin/rsync -avh " + f + " " + os.path.join(destDirName,os.path.basename(f))

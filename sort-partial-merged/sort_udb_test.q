#!/bin/sh

#$ -N psort_udb
#$ -q mendel.q
#$ -pe pe_1 1
#$ -V
#$ -cwd
#$ -l ram.c=120G

module use --append /usr/common/tig/Modules/modulefiles
module load taskfarmer/1.5
cd $SGE_O_WORKDIR 

# Setup task farmer
export PATH=$PATH:$(pwd)

export THREADS=1
export TF_SERVERS=`pwd`/servers
export SKIP_CHECK=1

runcommands.sh /global/projectb/scratch/sulsj/mgm_sort_partial_merged/code/sort_udb_test.lst
#!/usr/bin/env python
import sys
import glob
import os

srcDirName=sys.argv[1]
destDirName=sys.argv[2]

fnames = glob.glob(srcDirName + "/*.udbout")
print "num files = ", len(fnames)

for f in fnames:
    print "/global/projectb/scratch/sulsj/mgm_sort_partial_merged/code/msort5_parasort.py " + f + " " + os.path.join(destDirName,os.path.basename(f))

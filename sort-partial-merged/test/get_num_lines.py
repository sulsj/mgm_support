#!/usr/bin/env python
import sys

def get_num_line(fn):
    f = open(fn)
    lines = 0
    buf_size = 1024 * 1024
    read_f = f.read # loop optimization

    buf = read_f(buf_size)
    while buf:
        lines += buf.count('\n')
        buf = read_f(buf_size)
    f.close()

    return lines

if __name__ == '__main__':
    fname = sys.argv[1]
    print get_num_line(fname)

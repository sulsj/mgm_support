#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Written (W) 2012 Seung-Jin Sul
# Copyright (C) NERSC, LBL

"""

This script is to sort a file in parallel.
Originally written for processing Kostas' usearch output files.

"""
import sys
import glob
from collections import defaultdict
import operator
import math
import time
import os
import multiprocessing as mp
import Queue
import threading

g_numLineRead = 0
g_numLineWritten = 0
 
def load_file(f, d):
    """
    Read a file and load the contents in a dictionary
    so that it can collect the records with the same key.
    Here the key is gene_id.
    """
    print "Reading ", f
    fd = open(f, 'r')
    while 1:
        lines = fd.readlines(100000) ## batch readline
        if not lines:
            break
        for l in lines:
            ##
            ## Text newKey=new Text( String.format("%s %08.2f %f %d", a[0],
            ## Float.parseFloat(a[11]), // bitscore
            ## Float.parseFloat(a[10]), //evalue
            ## Integer.parseInt(a[3]) // alignment length
            ## )  );
            ##
            global g_numLineRead
            g_numLineRead += 1
            stripped = l.strip()                    
            t = stripped.split()
            k = t[0]
            d[k].append(stripped)
    fd.close()
    print "Num lines read = ", g_numLineRead
    

def load_dict(dirName, d):
    """
    Read a list of files and load the contents in a dictionary
    so that it can collect the records with the same key.
    Here the key is gene_id.
    """
    for f in glob.glob(dirName+'/*.udbout'):  
        print "Reading ", f
        fd = open(f, 'r')
        while 1:
            lines = fd.readlines(100000) ## batch readline
            if not lines:
                break
            for l in lines:
                ##
                ## Text newKey=new Text( String.format("%s %08.2f %f %d", a[0],
                ## Float.parseFloat(a[11]), // bitscore
                ## Float.parseFloat(a[10]), //evalue
                ## Integer.parseInt(a[3]) // alignment length
                ## )  );
                ##
                stripped = l.strip()                    
                t = stripped.split()
                k = t[0]
                d[k].append(stripped)
        fd.close()


def multiproc_worker(v, out_q):
    """
    Sorting worker for multiprocessing.
    Sort the list and put the sorted list in the queue so that the master
    can get the result.
    """
    ##
    ## Float.parseFloat(a[11]), // bitscore
    ## Float.parseFloat(a[10]), //evalue
    ## Integer.parseInt(a[3])   // alignment length
    ##
    ## Sorting
    ## 1:) Make sure you have only results from the same sequence of course
    ## (e.g. S_N403_prefert11DRAFT_30000001501)
    ## 2.) Sort by bitscore (highest first)
    ## 3) If two hits have the same bitscore, continue sorting by evalue
    ## (smallest first)
    ## 4) and if even that's the same, then continue sorting by the alignment
    ## length (longest first)
    ##  
    v.sort(key=lambda s: (s.split()[0],         # gene_id ASC
                          -float(s.split()[11]), # bitscore DESC
                          float(s.split()[10]),  # evalue ASC
                          -int(s.split()[3])    # alignlen DESC
           ))
    out_q.put(v)
    
        
def para_sort_multiproc(d, nprocs):
    # Each process will get 'chunksize' nums and a queue to put his out
    # dict into
    manager = mp.Manager()
    out_q = manager.Queue()    
    pool = mp.Pool(nprocs)    
    procs = []
    
    print "  Firing off workers"
    for k, v in d.items():
        proc = pool.apply_async(multiproc_worker, (v, out_q))
        procs.append(proc)
    
    ## collect results from the workers through the pool result queue
    print "  Collecting results from the workers"
    for proc in procs:
        sortedList = out_q.get()
        d[sortedList[0].split()[0]] = sortedList

    pool.close()
    pool.join()
    
    
def serial_sort(d):
    """
    Serial sort for benchmarking
    """
    for k, v in d.items():
        v.sort(key=lambda s: (s.split()[0],         # gene_id ASC
                              -float(s.split()[11]), # bitscore DESC
                              float(s.split()[10]),  # evalue ASC
                              -int(s.split()[3])    # alignlen DESC
             ))


def chunks(l, n): 
    """ Yield successive n-sized chunks from l.
    """
    for i in xrange(0, len(l), n):
        yield l[i:i+n]


class thread_worker(threading.Thread):
    """
    Get the list from the queue, sort it and push back to
    the result queue.
    """
    def __init__(self, tq, rq):
        threading.Thread.__init__(self)
        self.taskqueue = tq
        self.resultqueue = rq
        
    def run(self):
         while True:
            listToSort = self.taskqueue.get()
            if listToSort is None:
                break # reached end of queue
            listToSort.sort(key=lambda s: (s.split()[0],         # gene_id ASC
                                        -float(s.split()[11]), # bitscore DESC
                                        float(s.split()[10]),  # evalue ASC
                                        -int(s.split()[3])    # alignlen DESC
             ))
            self.resultqueue.put(listToSort)
            #self.taskqueue.task_done()


def para_sort_thread(d, nprocs):
    """
    Send each list in the dictionary, d, to the queue.
    Wait for the result.
    Get the sorted result and update the dict.
    """
    taskQueue = Queue.Queue()
    resultQueue = Queue.Queue()
    
    for i in range(nprocs):
        t = thread_worker(taskQueue, resultQueue)
        t.setDaemon(True)
        t.start()
        
    print "  Firing off workers"
    for k, v in d.items():
        taskQueue.put(v)
    
    #taskQueue.join()
    
    for i in range(nprocs):
        taskQueue.put(None) # add end-of-queue markers

    print "  Collecting results..."
    while not resultQueue.empty():
        sortedList = resultQueue.get()
        d[sortedList[0].split()[0]] = sortedList

    
if __name__ == "__main__":
    
    inFileName = sys.argv[1]
    outFileName = sys.argv[2]
    tmpOutFileName = outFileName + ".tmp"
   
    if not os.path.isfile(outFileName) and not os.path.isfile(tmpOutFileName):
    #if not os.path.isfile(outFileName):
        d = defaultdict(list)
        
        ##
        ## Read file into a dictionary
        ##
        start = time.clock()
        print "Loading dict..."
        #load_dict(dirName, d)
        load_file(inFileName, d)
        print "Loading done, ", time.clock() - start
                
        ##
        ## Sorting
        ##
        start = time.clock()
        print "Sorting..."
        print "  Cpu count = ", mp.cpu_count()
        ###################################
        #para_sort_multiproc(d, mp.cpu_count()/32)
        #serial_sort(d) 
        #para_sort_thread(d, mp.cpu_count())
        para_sort_thread(d, 16) ## For Mendel
        ###################################
        print "Sorting done, ", time.clock() - start
    
        ##
        ## Write to a new file.
        ## To do: This is the bottleneck
        ##
        start = time.clock()
        print "Writing..."
        outfd = open(tmpOutFileName, 'w')
        for k, v in d.items():
            for h in v:
                g_numLineWritten += 1
                t = h.split()
                line = "%s %08.2f %f %d %s" % (t[0], float(t[11]), float(t[10]), int(t[3]), h)
                outfd.write("%s\n" % line)
        outfd.close()
        print "Writing done, ", time.clock() - start
        
        ##
        ## QC
        ##
        if (g_numLineWritten == g_numLineRead):
            print "Check ok: read = file = %s, %d, written = %d" % (outFileName, g_numLineRead, g_numLineWritten) 
            os.rename(tmpOutFileName, outFileName)
            reportFileName = outFileName + "_OK_read_" + str(g_numLineRead) + "_written_" + str(g_numLineWritten)
            fd = open(reportFileName, "w")
            fd.write(str(g_numLineRead) + " " + str(g_numLineWritten) + '\n')
            fd.close()
        else:
            print "Check failed: file = %s, read = %d, written = %d" % (outFileName, g_numLineRead, g_numLineWritten) 
            errorFileName = tmpOutFileName + "_ERROR_read_" + str(g_numLineRead) + "_written_" + str(g_numLineWritten)
            os.rename(tmpOutFileName, errorFileName)
    else:
        print "Already done ", outFileName

#EOF 


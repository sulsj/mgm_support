#!/usr/bin/env python
import sys
import glob
import os

srcDirName=sys.argv[1]
destDirName=sys.argv[2]

fnames = glob.glob(srcDirName + "/*.udbout")
#print "num files = ", len(fnames)

for f in fnames:
    print "/global/projectb/scratch/sulsj/mgm_sort_partial_merged/code/compare_lines.py " + f + " " + destDirName

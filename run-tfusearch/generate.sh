#!/bin/sh


for i in $(ls -d *.split|sed 's/.split//') ; do
  CT=$(ls -d $SCRATCH/mgm/$i.split/[0-9]*|wc -l)
  for k in $(seq 1 20); do
    for j in $(seq 1 $CT); do
      echo "myusearch.sh $SCRATCH/mgm/$i.split/$j/$i.unassembled_illumina.faa $k"
    done
  done
done

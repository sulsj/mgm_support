#!/bin/sh

#$ -N usearch
#$ -q mendel.q
#$ -pe pe_16 16
#$ -V
##$ -l ram.c=8G

module use --append /usr/common/tig/Modules/modulefiles
module load taskfarmer/1.5
cd $SGE_O_WORKDIR 

# Setup task farmer
export PATH=$PATH:$(pwd)

export THREADS=4
export OMP_NUM_THREADS=4
export TF_SERVERS=`pwd`/servers
export DEBUGTF=1
export SKIP_CHECK=1


udborigin=/global/projectb/scratch/canon/database
udbdest=/scratch/sulsj
udbsize=(0 1164540646 1168644957 1162119999 1163011772 1178271134 1169261237 1191054773 1172078574 1169072745 1160711500 1181068266 1175347790 1171263800 1166534145 1181249218 1172071525 1197764258 1178075613 1175166168 1162070657)

for i in {1..20}
do
    udbname=img.faa.$i.udb
    if [ ! -e "$udbdest/$udbname" ] ; then
        echo "Copy $udborigin/$udbname to $udbdest/$udbname"
        mkdir -p $udbdest
        cp $udborigin/$udbname $udbdest/$udbname
    else
        UDBSZ=${udbsize[i]}
        FILESIZE=$(stat -c%s "$udbdest/$udbname")
        echo $UDBSZ $FILESIZE
        while [  $FILESIZE -lt $UDBSZ ]; do
            sleep 10
            FILESIZE=$(stat -c%s "$udbdest/$udbname")
        done
    fi
done



#tfrun
#runcommands.sh `pwd`/usearch.lst
runcommands.sh `pwd`/usearch_9999.lst

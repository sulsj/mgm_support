#!/bin/sh

input=$1
counter=$2

#if [ -e "$SCRATCH/WIDE" ] ; then
#  db=$SCRATCH/WIDE/database/img.faa.$counter.udb
#else
#  db=$BSCRATCH/database/img.faa.$counter.udb
#fi
#db=/global/projectb/scratch/canon/database/img.faa.$counter.udb


echo "check local udb"
if [ -e "/scratch/sulsj/img.faa.$counter.udb" ] ; then
    db=/scratch/sulsj/img.faa.$counter.udb
    echo "use /scratch/sulsj/img.faa.$counter.udb"
else
    db=/global/projectb/scratch/canon/database/img.faa.$counter.udb
    echo "use shared udb"
fi


output=$input.$counter.udbout

#temporary
if [ -e ${output}.tmp ];then 
  rm ${output}.tmp
fi

if [ -e $output.success  -a -s $output.success ]
then
	echo "File $output exists " >&2 
else
	/global/projectb/scratch/sulsj/mgm/catch.pl /global/projectb/scratch/sulsj/mgm/usearch32 -quiet -ublast $input --accel 0.8 --evalue 10e-1 --ka_dbsize 100000000 --userout ${output}.tmp --userfields query+target+id+alnlen+mism+opens+qlo+qhi+tlo+thi+evalue+bits+ql+tl --maxhits 50 --db $db -threads 4 --maxseqlength 2000
	RETVAL=$?
	if [ $RETVAL -ne 0 ];then
		exit $RETVAL
	else
		mv ${output}.tmp $output.success
	fi
fi



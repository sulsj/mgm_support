#!/bin/sh

#$ -N usearch
#$ -q mendel.q
#$ -pe pe_16 16
#$ -V

module use --append /usr/common/tig/Modules/modulefiles
module load taskfarmer/1.5
cd $SGE_O_WORKDIR 

# Setup task farmer
export PATH=$PATH:$(pwd)

export THREADS=16
export TF_SERVERS=`pwd`/servers
export SKIP_CHECK=1

tfrun

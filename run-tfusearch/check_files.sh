#!/bin/sh

echo "Usage: check_files.sh ID BASE PATTERN"

ID=$1
BASE=$2
PATT=$3

i=$ID
currdir=$(pwd)
count=0
#for i in $(ls -d *.split|sed 's/.split//') ; do
  #CT=$(ls -d $SCRATCH/mgm/$i.split/[0-9]*|wc -l)
  CT=$(ls -d $BASE/$i.split/[0-9]*|wc -l)
  echo "num splits = $CT"
  for d in $(seq 1 $CT); do
    cd $BASE/$i.split/$d
    #n=$(ls -al | grep $PATT | wc -l)
    n=$(find ./ -name '$PATT' | wc -l)
    #echo $n
    if [ $n -lt 20 ] ; then
      echo "$BASE/$i.split/$d ==> $n"
    fi
    count=$(( $count+1 ))
    cd ..
  done

cd $currdir
echo "total = $count"
#  for k in $(seq 1 20); do
#    for j in $(seq 1 $CT); do
#      #echo "myusearch.sh $SCRATCH/mgm/$i.split/$j/$i.unassembled_illumina.faa $k"
#      echo "$BASE/myusearch.sh $BASE/$i.split/$j/$i.unassembled_illumina.faa $k"
#    done
#  done
#done

#!/bin/bash -l
dir=$1
for i in {1..20}
do
  n=$(find $dir -name "*.$i.udbout.success" | wc -l)
  echo $i $n
done

#!/bin/sh

#$ -N usearch
#$ -q mendel.q
#$ -pe pe_16 16
#$ -V

module use --append /usr/common/tig/Modules/modulefiles
module load taskfarmer/1.5
cd $SGE_O_WORKDIR 

# Setup task farmer
export PATH=$PATH:$(pwd)

export THREADS=4
export OMP_NUM_THREADS=4
export TF_SERVERS=`pwd`/servers
export DEBUGTF=1
export SKIP_CHECK=1

tfrun
#runcommands.sh `pwd`/usearch.lst

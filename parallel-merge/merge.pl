#!/usr/bin/env perl

$BASE=$ARGV[0];
$id=$ARGV[1];
$ext=$ARGV[2]; #"hmmout"
$max=$ARGV[3];
$step=$ARGV[4];
$udbs=20;

if (! defined $ENV{STEP}){
  start_server();
  exit;
}

# ID
$_=<STDIN>;
s/> *//;
($start,$stop)=split;
#chdir($BASE);
$PATTERN="$BASE/%d.split/%d/%d.unassembled_illumina.faa.%s";
$PATUDB="$BASE/%d.split/%d/%d.unassembled_illumina.faa.%d.%s.success";
$OUTFMT="$BASE/%d/%d.unassembled_illumina.faa.%d.%s";
mkdir "$BASE/$id" if (! -e "$BASE/$id");
$out=sprintf $OUTFMT,$id,$id,$start,$ext;
open(O,"> $out");
foreach $i ($start..$stop) {
  if ($ext eq 'udbout'){
    for ($j=1;$j<=20;$j++){
      $file=sprintf $PATUDB,$id,$i,$id,$j,$ext;
      #print "Adding $file\n";
      print O dumpfile($file);
      push @list,$file;
    }
  }
  else{
    $file=sprintf $PATTERN,$id,$i,$id,$ext;
    print O dumpfile($file);
    push @list,$file;
  }
}
close O;


$list=join " ",@list;
#print `cat $list > $out`;


sub start_server {
  $input="$id.$ext.in";
  if (! -e $input) {
    generate_input($input);
  }
  my $base=`pwd`;
  chomp $base;
  $ENV{BATCHSIZE}=1;
  $ENV{SERVER_TIMEOUT}=1700;
  system("tfrun","-i",$input,$0,$base,$id,$ext,$max);
  print "$?\n";
  
}

sub generate_input {
  my $in = shift;

  open(I, ">$in");
  my $i;
  for ($i=1;$i<$max;$i+=$step){
    printf I "> %d %d\n",$i,$i+$step-1;
  }
  close I; 
}


sub dumpfile {
  my $file=shift;
  my $buffer="";
  open(F,$file);
  while(<F>){
    $buffer.=$_;
  }
  close F;
  return $buffer;
}

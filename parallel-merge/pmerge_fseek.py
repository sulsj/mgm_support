#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Written (W) 2012 Seung-Jin Sul
# Copyright (C) NERSC, LBL

"""
 

"""
import multiprocessing as mp
import time
import sys
import glob
import os

dirName = sys.argv[1]
outFileName = sys.argv[2]
#outFd = open(outFileName, "w")

def para_reader(fn, offset, q):
    ''' Read the assigned file and write the contents to outFileName with offset
    '''
 
    print "Processing f", fn, ", size = ", os.path.getsize(fn), ", offset = ", offset[fn]
    try:
        outFd = open(outFileName,'r+b')
    except IOError:
        outFd = open(outFileName,'wb')
    outFd.seek(offset[fn])
    
    fd = open(fn, 'r')
    while 1:
        lines = fd.readlines(100000)
        if not lines:
            break
        for l in lines:
            outFd.write(l)
    fd.close()
    outFd.close()
        
    print fn, " done."
    return fn

def writer(q):
    ''' listens for messages on the q for finish '''

    #f = open(outFileName, 'w') 
    while 1:
        m = q.get()
        if m == 'kill':
            break

def main():
    ## must use Manager queue here, or will not work
    manager = mp.Manager()
    q = manager.Queue()    
    pool = mp.Pool(mp.cpu_count() + 2)

    ## put writer to work first
    watcher = pool.apply_async(writer, (q,))

    ## fire off workers
    jobs = []
    fNames = glob.glob(dirName+'/*.sorted')
    offset = {}
    accSz = 0
    for fn in fNames:
        fSize = os.path.getsize(fn)
        offset[fn] = accSz
        accSz += fSize
    print offset
    
    for fn in fNames:  
        job = pool.apply_async(para_reader, (fn, offset, q))
        jobs.append(job)

    ## collect results from the workers through the pool result queue
    for job in jobs: 
        job.get()
        
    ## To do: how can I ensure all readers are done??
    ## job.get() is blocking op.
    ## http://docs.python.org/2/library/multiprocessing.html
    
    ## now we are done, kill the writer
    print "Kill the writer process."
    q.put('kill')
    pool.close()
    pool.join()
    
if __name__ == "__main__":
   main()

## EOF
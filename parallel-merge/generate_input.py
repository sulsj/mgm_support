#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Written (W) 2012 Seung-Jin Sul
# Copyright (C) NERSC, LBL

"""
 

"""
import time
import sys
import glob
import os

inputFileName =  sys.argv[1]
targetDir = sys.argv[2]
fileNamePattern = sys.argv[3]

fNames = glob.glob(targetDir + '/*.' + fileNamePattern)
print " ".join(fNames)
fd = open(inputFileName, "w")
offset = 0
for fn in fNames:
    fSize = os.path.getsize(fn)
    fd.write("> " + fn + " " + str(fSize) + " " + str(offset) + '\n')
    offset += fSize
fd.close()

## EOF
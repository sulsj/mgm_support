#!/usr/bin/env python
"""


"""
import sys

inFileName = sys.argv[1]  # partial out
offset = sys.argv[2] # file offset to write
outFileName = sys.argv[3]

try:
    outFd = open(outFileName,'r+b')
except IOError:
    outFd = open(outFileName,'wb')
outFd.seek(int(offset))

fd = open(inFileName, 'r')
while 1:
    lines = fd.readlines(100000)
    if not lines:
        break
    for l in lines:
        outFd.write(l)
fd.close()
outFd.close()

## EOF
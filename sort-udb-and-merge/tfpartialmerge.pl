#!/usr/bin/env perl

$BASE=$ARGV[0];
$id=$ARGV[1];
$ext=$ARGV[2]; #"hmmout"
$max=$ARGV[3];
$step=$ARGV[4];
$udbs=20;

if (! defined $ENV{STEP}){
  start_server();
  exit;
}

# ID
$_=<STDIN>;
s/> *//;
($start,$stop)=split;
#chdir($BASE);
$PATTERN="$BASE/%d.split/%d/%d.unassembled_illumina.faa.%s";
$PATUDB="$BASE/%d.split/%d/%d.unassembled_illumina.faa.%d.%s.success";
$OUTFMT="$BASE/%d/%d.unassembled_illumina.faa.%d.%s";
$OUTTEMPFMT="$BASE/%d/%d.unassembled_illumina.faa.%d.%s.tmp";
$SORTEDOUTFMT="$BASE/%d.split/%d/%d.unassembled_illumina.faa.%s.success.sorted";

mkdir "$BASE/$id" if (! -e "$BASE/$id");
$out=sprintf $OUTFMT,$id,$id,$start,$ext;
$tempout=sprintf $OUTTEMPFMT,$id,$id,$start,$ext;

if ( ! -e $out ) {
    open(O,"> $tempout");
    foreach $i ($start..$stop) {
      if ($ext eq 'udbout'){
        #for ($j=1;$j<=20;$j++){
        #  $file=sprintf $PATUDB,$id,$i,$id,$j,$ext;
        #  #print "Adding $file\n";
        #  print O dumpfile($file);
        #  push @list,$file;
        #}
        $sortedout=sprintf $SORTEDOUTFMT, $id, $i, $id, $ext;
        if ( ! -e $sortedout ) {
          #$file=~s/.success//;
          print STDERR "Sorted file not found, $sortedout";
        }
        else {
            print O dumpfile($sortedout);
            push @list,$sortedout;
        }
      }
      else{
        $file=sprintf $PATTERN,$id,$i,$id,$ext;
        print O dumpfile($file);
        push @list,$file;
      }
    }
    close O;
    
    while (1) {
        if (open my $fh, "+<", $tempout) {
            #print "available\n";
            close $fh;
            rename $tempout, $out;
            break;
        }
        else {
          print $^E == 0x20 ? "in use by another process\n" : "$!\n";
          sleep 5
        }
    }
    #rename $tempout, $out;
}   
else {
    print "already done with $out"
}

$list=join " ",@list;
#print `cat $list > $out`;


sub start_server {
  $input="$id.$ext.tfmerge.in";
  if (! -e $input) {
    generate_input($input);
  }
  my $base=`pwd`;
  chomp $base;
  $ENV{BATCHSIZE}=1;
  $ENV{SERVER_TIMEOUT}=1700;
  system("tfrun","-i",$input,$0,$base,$id,$ext,$max);
  print "tfrun return = $?\n";
  
}

sub generate_input {
  my $in = shift;

  open(I, ">$in");
  my $i;
  for ($i=1;$i<$max;$i+=$step){
    printf I "> %d %d\n",$i,$i+$step-1;
  }
  close I; 
}


sub dumpfile {
  my $file=shift;
  my $buffer="";
  open(F,$file);
  while(<F>){
    $buffer.=$_;
  }
  close F;
  return $buffer;
}

#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Written (W) 2012 Seung-Jin Sul
# Copyright (C) NERSC, LBL

"""

This is wriiten for testing sorting in batch. NOT USED ANY MORE.

"""
import sys
import glob
from collections import defaultdict
import operator
import time
import os

def sort_udb_outputs(dirName):
    d = defaultdict(list)
    print "    Working in ", dirName
    
    ## Check done.sorted
    if not os.path.exists(os.path.join(dirName, "done.sort")):
        outFileName = ""
        start = time.clock()
        print "    Loading..."
        ###############################################
        ## Be cautious on the output file name pattern
        ###############################################
        for f in glob.glob(dirName+"/*.udbout.success"): ## Kostas' outputs
        #for f in glob.glob(dirName+'/*.db*'): ## Amrita's outputs
            print "    Reading ", f
            outFileName = f
            fd = open(f, 'r')
            while 1:
                lines = fd.readlines(100000) ## batch readline
                if not lines:
                    break
                for l in lines:
                    k = l.strip().split()[0]
                    v = l.strip()
                    d[k].append(v)
            fd.close()
        print "    Loading done, ", time.clock() - start
    
        start = time.clock()
        
        print "    Sorting..."
        toks = outFileName.split('/')[-1].split('.')
        outFileName = ".".join(toks[:3]) + ".udbout.success.sorted"
        print "    Outfile = ", dirName+'/'+outFileName 
        outfd = open(os.path.join(dirName, outFileName), 'w')
        for k, v in d.items():
            ##############################################################
            ## Be cautious on the field location for gene_id and bit score
            ##############################################################
            v.sort(key=lambda s: (s.split()[0], -float(s.split()[-1]))) ## sort by gene_id ASC, bitscore DESC
            for h in v:
                outfd.write("%s\n" % h)
        outfd.close()
        print "    Sorting done, ", time.clock() - start
        
        ## Create sorting done file
        print "    Create done file = ", dirName+"/done.sorted"
        donefd = open(os.path.join(dirName, "done.sort"), 'w')
        donefd.write("done\n")
        donefd.close()
    else:
        print "    Already done in ", dirName


if __name__ == "__main__":
    
    rootDir = sys.argv[1] 
    
    # Get *.split list and sort
    for splitRoot in glob.glob(rootDir+"/*.split"): 
        if os.path.isdir(splitRoot):
            print splitRoot
            for splitDir in glob.glob(splitRoot+'/*[0-9]'): ## split dir names
                if os.path.isdir(splitDir):
                    print " ", splitDir
                    sort_udb_outputs(splitDir)
#EOF 

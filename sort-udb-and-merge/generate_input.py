#!/usr/bin/env python
"""


"""
import time
import sys
import glob
import os

inputFileName =  sys.argv[1]
targetDir = sys.argv[2]
fileNamePattern = sys.argv[3]

fNames = glob.glob(targetDir + '/*.' + fileNamePattern)
print " ".join(fNames)
fd = open(inputFileName, "w")
offset = 0
for fn in fNames:
    fSize = os.path.getsize(fn)
    fd.write("> " + fn + " " + str(fSize) + " " + str(offset) + '\n')
    offset += fSize
fd.close()

## EOF
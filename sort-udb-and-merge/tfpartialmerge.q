#!/bin/sh

#PBS -N packer
#PBS -q debug
#PBS -l mppwidth=240,walltime=00:30:00
##PBS -A m1045
#PBS -V

#qsub -I -V -q interactive -l mppwidth=48 

cd $PBS_O_WORKDIR 

#module load blast taskfarmer hmmsort hmmer
ID=9213
STEP=50

#CT=$(ls $ID.split|grep ^[0-9]|wc -l)
#echo $CT

#for ext in hmmout rpsout udbout ; do
#for ext in usearch ; do
for ext in udbout ; do
  #if [ ! -e "done.$ID.$ext.in" ] ; then
    #CT=$(ls $ID.$ext.split|grep ^[0-9]|wc -l) # Amrita's
    CT=$(ls $ID.split|grep ^[0-9]|wc -l)
    echo "CT = $CT"
    `pwd`/tfpartialmerge.pl `pwd` $ID $ext $CT $STEP
  #fi
done
#`pwd`/merge.pl `pwd` $ID hmmout 15000 50
#`pwd`/merge.pl `pwd` $ID rpsout 15000 50
#`pwd`/merge.pl `pwd` $ID udbout 15000 50

#-rw-r--r-- 1 kmavromm kmavromm  20442542 Oct  9 10:17 9535.unassembled_illumina.faa.1.udbout.success


#!/usr/bin/env perl

$BASE=$ARGV[0];
$id=$ARGV[1];
$ext=$ARGV[2]; # hmmout | rpsout | udbout # Kostas'
$max=$ARGV[3];
$step=$ARGV[4];
#$udbs=20;

if (! defined $ENV{STEP}){
  start_server();
  exit;
}

# ID
$_=<STDIN>;
s/> *//;
($start,$stop)=split;
#chdir($BASE);
$PATTERN="$BASE/%d.split/%d/%d.unassembled_illumina.faa.%s";

# Kostas' 
$PATUDB="$BASE/%d.split/%d/%d.unassembled_illumina.faa.%d.%s.success";
$UDBOUTDIR="$BASE/%d.split/%d";
$SORTEDOUTFMT="$BASE/%d.split/%d/%d.%s.out.db.%d.sorted"; ## ex) 9486.usearch.out.db.sorted

# Amrita's
#$PATUDB="$BASE/%d.%s.split/%d/%d.%s.out.db%d"; ## ex) 9486.usearch.out.db1
#$UDBDIR"$BASE/%d"
#$SORTEDOUTFMT="$BASE/%d/%d.%s.out.db.%d.sorted"; ## ex) 9486.usearch.out.db.sorted
#$TEMPOUTFMT="$BASE/%d/%d.%s.out.db.%d.all"; ## ex) 9486.usearch.out.db.all

$OUTFMT="$BASE/%d/%d.unassembled_illumina.faa.%d.%s";
mkdir "$BASE/$id" if (! -e "$BASE/$id");
$out=sprintf $OUTFMT,$id,$id,$start,$ext;
open(O,"> $out");
foreach $i ($start..$stop) {
  if ($ext eq 'udbout') {
    
    ###################
    ### sort and merge
    ###################
    ##  $ ./msort4.py <udb files dir> <output file>.
    $udbFileDir=sprintf $UDBOUTDIR, $id, $i;
    print "udbFileDir: $udbFileDir\n";
    $sortedout=sprintf $SORTEDOUTFMT, $id, $i, $id, $ext, $start;
    print "Sortedout: $sortedout\n";
    system("python $BASE/msort4_marcel.py $udbFileDir $sortedout");
    if ($? == -1) {
        print "failed to execute: $!\n";
    }
    elsif ($? & 127) {
        printf "child died with signal %d, %s coredump\n",
        ($? & 127),  ($? & 128) ? 'with' : 'without';
    }
    else {
        printf "child exited with value %d\n", $? >> 8;
    }
    
    print O dumpfile($sortedout);
    push @list,$sortedout;
  }
  else {
    $file=sprintf $PATTERN,$id,$i,$id,$ext;
    print O dumpfile($file);
    push @list,$file;
  }
}
close O;


$list=join " ",@list;
#print `cat $list > $out`;


sub start_server {
  $input="$id.$ext.in";
  if (! -e $input) {
    generate_input($input);
  }
  my $base=`pwd`;
  chomp $base;
  $ENV{BATCHSIZE}=1;
  $ENV{SERVER_TIMEOUT}=1700;
  system("tfrun","-i",$input,$0,$base,$id,$ext,$max);
  print "$?\n";
  
}

sub generate_input {
  my $in = shift;

  open(I, ">$in");
  my $i;
  for ($i=1;$i<$max;$i+=$step){
    printf I "> %d %d\n",$i,$i+$step-1;
  }
  close I; 
}


sub dumpfile {
  my $file=shift;
  my $buffer="";
  open(F,$file);
  while(<F>){
    $buffer.=$_;
  }
  close F;
  return $buffer;
}

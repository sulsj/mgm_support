#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Written (W) 2012 Seung-Jin Sul
# Copyright (C) NERSC, LBL

"""

This is written for soting Amrita's usearch outputs.
The inputs are a list of udb output files for UDB splits.

"""
import sys
import glob
from collections import defaultdict
import operator
import time

if __name__ == "__main__":
    
    dirName = sys.argv[1] 
    d = defaultdict(list)
    print "Working in ", dirName
    
    start = time.clock()
    print "Loading..."
    for f in glob.glob(dirName+'/*.db*'): ## filenames for *.db??
        print "Reading ", f
        fd = open(f, 'r')
        while 1:
            lines = fd.readlines(100000) ## batch readline
            if not lines:
                break
            for l in lines:
                k = l.strip().split()[0]
                v = l.strip()
                d[k].append(v)
        fd.close()
    print "Loading done, ", time.clock() - start

    start = time.clock()
    print "Sorting..."
    outf = open(sys.argv[2], 'w')
    for k, v in d.items():
        v.sort(key=lambda s: (s.split()[0], -float(s.split()[-1]))) ## sort by gene_id ASC, bitscore DESC
        for h in v:
            outf.write("%s\n" % h)
    outf.close()
    print "Sorting done, ", time.clock() - start
    
    ## Create sorting done file
    fd = open("done.sorted", "w")
    fd.write("done\n")
    fd.close()
    
#EOF 

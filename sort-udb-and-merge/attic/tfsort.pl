#!/usr/bin/env perl

$BASE=$ARGV[0]; # loc
$id=$ARGV[1];   # dir name
$ext=$ARGV[2];  # hmmout | rpsout | udbout


if (! defined $ENV{STEP}){
  start_server();
  exit;
}

# ID
$_=<STDIN>;
s/> *//;
#$PATTERN="$BASE/%d.split/%d/%d.unassembled_illumina.faa.%s";
#$PATUDB="$BASE/%d.split/%d/%d.unassembled_illumina.faa.%d.%s.success";
$OUTFMT="$BASE/%d/%d.unassembled_illumina.faa.%s.merged";
$outFileName=sprintf $OUTFMT, $id, $id, $ext;
($inFileName, $fSize, $offset)=split;
system("python $BASE/pmerge.py $inFileName $offset $outFileName");


$list=join " ",@list;
#print `cat $list > $outFileName`;


sub start_server {
  $input="$id.$ext.in";
  if (! -e $input) {
    generate_input($input);
  }
  my $base=`pwd`;
  chomp $base;
  $ENV{BATCHSIZE}=1;
  $ENV{SERVER_TIMEOUT}=1700;
  system("tfrun", "-i", $input, $0, $base, $id, $ext);
  print "$?\n";
  
}

sub generate_input {
  my $in = shift;
  my $base=`pwd`;
  chomp $base;
  system("python $base/generate_input.py $in $base/$id $ext");
}


#sub dumpfile {
#  my $file=shift;
#  my $buffer="";
#  open(F,$file);
#  while(<F>){
#    $buffer.=$_;
#  }
#  close F;
#  return $buffer;
#}

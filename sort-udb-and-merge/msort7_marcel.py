#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Written (W) 2012 Seung-Jin Sul
# Copyright (C) NERSC, LBL

"""

This is written for sorting UDB partial outputs and merge.

"""
import sys
import glob
from collections import defaultdict
import operator
import time
import os

g_numLineRead = 0
g_numLineWritten = 0

if __name__ == "__main__":
    
    dirName = sys.argv[1]
    outFileName = sys.argv[2]
    tmpOutFileName = outFileName + ".tmp"
    
    d = defaultdict(list)
    print "Working in ", dirName
    
    #if not os.path.isfile(os.path.join(dirName, "done.sort.from.msort4")):
    if not os.path.isfile(outFileName) and not os.path.isfile(tmpOutFileName):
        start = time.clock()
        print "Loading..."
        files = glob.glob(dirName+'/*.udbout.success')
        if len(files) != 20:
            print >> sys.stderr, "Cannot find 20 udb output files in %s." % (dirName) 
            print >> sys.stderr, "UDB output files = %s" % (str(files))
            print >> sys.stderr, "Number of files = %d" % (len(files))
            
        for f in files: 
            print "Reading ", f
            fd = open(f, 'r')            
            while 1:
                lines = fd.readlines(100000) ## batch readline
                if not lines:
                    break
                for l in lines:
                    global g_numLineRead
                    g_numLineRead += 1
                    ##
                    ## Text newKey=new Text( String.format("%s %08.2f %f %d", a[0],
                    ## Float.parseFloat(a[11]), // bitscore
                    ## Float.parseFloat(a[10]), //evalue
                    ## Integer.parseInt(a[3]) // alignment length
                    ## )  );
                    ##
                    stripped = l.strip()                    
                    t = stripped.split()
                    k = t[0]
                    try:
                        v = "%s %08.2f %f %d %s" % (t[0], float(t[11]), float(t[10]), int(t[3]), stripped)
                    except Exception as e:
                        print >> sys.stderr, type(e)
                        print >> sys.stderr, "line = ", l
                        print >> sys.stderr, "t = ", t
                        print >> sys.stderr, "dirName = ", dirName
                        print >> sys.stderr, "f = ", f
                        #sys.exit(1)
                        continue
                    d[k].append(v)
            fd.close()
        print "Loading done, ", time.clock() - start
    
        start = time.clock()
        print "Sorting..."
        #outFileName = sys.argv[2]+".tmp"
        #outf = open(outFileName, 'w')
        for k, v in d.items():
            ##
            ## Float.parseFloat(a[11]), // bitscore
            ## Float.parseFloat(a[10]), //evalue
            ## Integer.parseInt(a[3])   // alignment length
            ##
            ## Sorting
            ## 1:) Make sure you have only results from the same sequence of course
            ## (e.g. S_N403_prefert11DRAFT_30000001501)
            ## 2.) Sort by bitscore (highest first)
            ## 3) If two hits have the same bitscore, continue sorting by evalue
            ## (smallest first)
            ## 4) and if even that's the same, then continue sorting by the alignment
            ## length (longest first)
            ##
            v.sort(key=lambda s: (s.split()[0],         # gene_id ASC
                                  -float(s.split()[1]), # bitscore DESC
                                  float(s.split()[2]),  # evalue ASC
                                  -int(s.split()[3])    # alignlen DESC
                   ))
            #for h in v:
                #g_numLineWritten += 1
                #outf.write("%s\n" % h)
        #outf.close()
        print "Sorting done, ", time.clock() - start
        
        ##
        ## Write to a new file.
        ## To do: This is the bottleneck
        ##
        start = time.clock()
        print "Writing..."
        outfd = open(tmpOutFileName, 'w')
        for k, v in d.items():
            for h in v:
                g_numLineWritten += 1
                #t = h.split()
                #line = "%s %08.2f %f %d %s" % (t[0], float(t[11]), float(t[10]), int(t[3]), h)
                outfd.write("%s\n" % h)
        outfd.close()
        print "Writing done, ", time.clock() - start
        
        ##
        ## QC
        ##
        if (g_numLineWritten == g_numLineRead):
            print "Check ok: read = file = %s, %d, written = %d" % (outFileName, g_numLineRead, g_numLineWritten) 
            os.rename(tmpOutFileName, outFileName)
            reportFileName = outFileName + "_OK_read_" + str(g_numLineRead) + "_written_" + str(g_numLineWritten)
            fd = open(reportFileName, "w")
            fd.write(str(g_numLineRead) + " " + str(g_numLineWritten) + '\n')
            fd.close()
        else:
            print "Check failed: file = %s, read = %d, written = %d" % (outFileName, g_numLineRead, g_numLineWritten) 
            errorFileName = tmpOutFileName + "_ERROR_read_" + str(g_numLineRead) + "_written_" + str(g_numLineWritten)
            os.rename(tmpOutFileName, errorFileName)
            
        ## Create sorting done file
        #fd = open(os.path.join(dirName, "done.sort.from.msort4"), "w")
        #fd.write("done\n")
        #fd.close()
        
        #time.sleep(1)
        #os.rename(outFileName, outFileName[:-4])

    else:
        print "Already done in ", dirName
#EOF 
 